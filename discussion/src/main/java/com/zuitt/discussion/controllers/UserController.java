package com.zuitt.discussion.controllers;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class UserController {
    @Autowired
    UserService userService;
    //Get all users
    @RequestMapping(value="/users",method = RequestMethod.GET)
    public ResponseEntity<Object>getUsers(){
        return new ResponseEntity<>(userService.getUser(), HttpStatus.OK);
    }
    //Create users
    @RequestMapping(value="/users", method = RequestMethod.POST)
    public ResponseEntity<Object>createUsers(@RequestBody User user){
        userService.createUser(user);
        return new ResponseEntity<>("User created successfully",HttpStatus.CREATED);
    }
    //Update users
    @RequestMapping(value="/users/{id}",method = RequestMethod.PUT)
    public ResponseEntity<Object> updateUsers( @PathVariable Long id, @RequestBody User user){
        return userService.updateUser(id,user);
    }
    //Delete users
    @RequestMapping(value="/users/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object>deleteUsers(@PathVariable Long id){
        return new ResponseEntity<>(userService.deleteUser(id),HttpStatus.OK);
    }

}
