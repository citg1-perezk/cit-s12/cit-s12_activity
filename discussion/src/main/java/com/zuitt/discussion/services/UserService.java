package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import org.springframework.http.ResponseEntity;

public interface UserService {
    void createUser(User user);
    Iterable<User> getUser();
    ResponseEntity deleteUser(Long id);
    ResponseEntity updateUser(Long id, User user);
}
