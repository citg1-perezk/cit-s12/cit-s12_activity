// contains the business logic concerned with a particular object   in the class
package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

// will allow us to use the CRUD repository methods inherited from the CRUDRepository
@Service
public class PostServiceImpl implements PostService{
    //An object cannot be instantiated from interfaces.
    //@Autowired allow us to use the interface as if it was an instance marked as @Repository contains methods for database manipulation
    @Autowired
    private PostRepository postRepository;
    //Create Post
    public void createPost(Post post){
        postRepository.save(post);
    }
    //Get all posts
    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }
    //delete post
    public ResponseEntity deletePost(Long id){
        postRepository.deleteById(id);
        return new ResponseEntity<>("Post deleted successfully.", HttpStatus.OK);
    }
    //update post
    public ResponseEntity updatePost(Long id, Post post){
        //find post to update
        Post postForUpdate=postRepository.findById(id).get();
        //Saving and Updating the title and content
        postForUpdate.setTitle(post.getTitle());
        postForUpdate.setContent(post.getContent());
        postRepository.save(postForUpdate);
        return new ResponseEntity("Post Updated successfully", HttpStatus.OK);
    }

}
