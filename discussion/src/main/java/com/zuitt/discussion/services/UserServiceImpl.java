package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserRepository userRepository;
    //Get all users
    public Iterable<User> getUser() {
        return userRepository.findAll();
    }
    public void createUser(User user) {
        userRepository.save(user);
    }
    public ResponseEntity updateUser(Long id, User user) {
        User userUpdate=userRepository.findById(id).get();
        userUpdate.setUsername(user.getUsername());
        userUpdate.setPassword(user.getPassword());
        userRepository.save(userUpdate);
        return new ResponseEntity<>("Post Updated Successfully",HttpStatus.OK);
    }
    public ResponseEntity deleteUser(Long id) {
        userRepository.deleteById(id);
        return new ResponseEntity<>("Post Deleted Successfully", HttpStatus.OK);
    }


}
